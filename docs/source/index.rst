.. hybridvut documentation master file, created by
   sphinx-quickstart on Mon Mar 28 10:05:44 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hybridvut!
=====================================

.. toctree::
   :maxdepth: 2

   intro
   concept
   hybridization
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
