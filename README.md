# hybridvut

This package provides functionalities to deal with hybrid IO-LCA based on the Make and Use framework.

Key features are:
* hybridization of a foreground and a background system
* unit handling using [iam_units](https://github.com/IAMconsortium/units) ([pint](https://pint.readthedocs.io/en/stable/) units)
* functions for data handling (e.g. region aggregation, RAS)
* basic IO calculations (e.g. transaction and multiplier matrices)
* preprocessing to bring raw data into the right format (i.e. message-ix data)

Additional hybridization procedures might be implemented in future work. Furthermore, functions can be also applied just to a single system.


## Installation

hybridvut is registered at PyPI. You can install it in your pathon environmnet by:

```bash
pip install hybridvut

```

The source-code is available as a [GitLab repository](https://gitlab.com/exiobase/hybridvut).
You can also fork and install it.

## Documentation

You can find a more detailed documentation [here](https://exiobase.gitlab.io/hybridvut/).
 

